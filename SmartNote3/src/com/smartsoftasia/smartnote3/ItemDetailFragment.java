package com.smartsoftasia.smartnote3;

import java.io.File;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.smartsoftasia.smartnote3.database.DataBaseRepository;
import com.smartsoftasia.smartnote3.note.Note;

/**
 * A fragment representing a single Item detail screen. This fragment is either
 * contained in a {@link ItemListActivity} in two-pane mode (on tablets) or a
 * {@link ItemDetailActivity} on handsets.
 */
public class ItemDetailFragment extends Fragment {
	/**
	 * The fragment argument representing the item ID that this fragment
	 * represents.
	 */
	public static final String ARG_ITEM_ID = "item_id";
	private static final String BOOL_IS_CHANGE = "boolIsChange";
	public static int RESULT_LOAD_IMAGE = 1;


	/**
	 * The dummy content this fragment is presenting.
	 */
	private Note mItem;
	private EditText textViewResume;
	private TextView textViewDate;
	private ImageView myImage;
	private String pathPicture;
	private Bitmap myBitmap;
	private DataBaseRepository dataBaseRepository;
	private boolean isChange;


	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public ItemDetailFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	       //set you initial fragment object 
		
		if(savedInstanceState == null){
			isChange = false;
		}
		else {
			isChange = savedInstanceState.getBoolean(BOOL_IS_CHANGE);
		}
		if (getArguments().containsKey(ARG_ITEM_ID)) {
		// Load the dummy content specified by the fragment
		// arguments. In a real-world scenario, use a Loader
		// to load content from a content provider.
		//	mItem = DummyContent.ITEM_MAP.get(getArguments().getString(
		//			ARG_ITEM_ID));
		    Bundle i = getArguments();
			mItem = i.getParcelable(ARG_ITEM_ID);
		}
		
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		return initialisation( inflater,  container, savedInstanceState);
	}
	
	@Override
    public void onSaveInstanceState(Bundle outState)
    {
		isChange = asChange();
		outState.putBoolean(BOOL_IS_CHANGE, isChange);
		
		mItem.setPicturePath(pathPicture);
		mItem.setResume(textViewResume.getText().toString());
		
    }
	
	@Override
	public void onDetach() {
		super.onDetach();
		
		exitView();
	}
	
	/******************************************************************************************************************************************************/
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
		inflater.inflate(R.menu.item_detail_activity, menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		    switch (item.getItemId()) {
		        case android.R.id.home:
		        	//finishview();
		            return true;
		            //appuie sur le bouton save
		        case R.id.menu_note_buttonCheck :
		        	saveText(); //on appler la fonction pour sauvegarder
		        	return true;
		        	
		        	//appuis sur le bouton image
		        case R.id.menu_note_itemCamera :
		        	setNewImage();
		        	return true;
		    }
		    return super.onOptionsItemSelected(item);
	}
	
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
	    super.onCreateContextMenu(menu, v, menuInfo);
	    menu.setHeaderTitle("Menu");
	    android.view.MenuInflater inflater = (android.view.MenuInflater) getActivity().getMenuInflater();
	    inflater.inflate(R.menu.menu_contextual_intentnote, (android.view.Menu) menu);
    }
	

	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		//get the ID of the menu, the first call is the first execute
		if(item.getGroupId() == R.id.group_menudetail){
		// we get the ID and we compare
			switch (item.getItemId()) {
			
				case R.id.itemOpen: //start new activity
					setNewImage();
					return true;
	
				case R.id.itemDelete: //delete the item
					deleteImage();
					return true;
					
				case R.id.itemCancel: //do nothing
					Log.v("detail",String.valueOf(item.getGroupId()));
					return true;
			}
		}
		return super.onContextItemSelected(item);
	}
	
	
	/*****************************************************************************************************************************************************/
	

	
	private View initialisation(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		
		View rootView = inflater.inflate(R.layout.fragment_item_detail,
				container, false);
		
		// Show the dummy content as text in a TextView.
		if (mItem != null) {
			
			textViewResume = (EditText) rootView.findViewById(R.id.item_detail);
			textViewDate = (TextView) rootView.findViewById(R.id.textViewDate);
			myImage = (ImageView) rootView.findViewById(R.id.imgView);
			
	    	//initialisation des variables
	    	myBitmap = null;
	    	
	    	// on v�rifie la vertion
	    //	if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
	    //		getActionBar().setDisplayHomeAsUpEnabled(true); //enable the up buton
	    //	}

	    
	    	
	    	
	    	getActivity().setTitle(mItem.getTitre());
	    	textViewDate.setText(mItem.getDate());
	    	textViewResume.setText(mItem.getResume());
	    	//getActionBar().setDisplayHomeAsUpEnabled(true); //enable the up buton
	    	if(mItem.getPicturePath().isEmpty())//on regarde si le chemain de l'image est vide
		    {
	    		myImage.setVisibility(View.GONE);//on masque le layout
		    }
	    	else {
	    		setImageOnImageView(mItem.getPicturePath());
	    	}
	  
	    	pathPicture = mItem.getPicturePath();
		}
		//save the list to the contextual menu
		registerForContextMenu(myImage);		

    	//on cr�e la base de donne
        dataBaseRepository = new DataBaseRepository(rootView.getContext()); //create nex database
		
		return rootView;
		
	}
	
	private void setImageOnImageView(String path){
		//option pour alleger l'image
        BitmapFactory.Options options = new BitmapFactory.Options(); //on cr�e une option
        options.inSampleSize = 2; //on divise la taille par deux
        File tmpFile = new File(path); //on obtien le fichier
        if(((tmpFile.length()/1024)/1024 )>= 1 ){ //on regarde si il est sup�rieur a 2mO
		myBitmap = BitmapFactory.decodeFile(path, options);
        }
        else{ //si l'image est < a 2mo
    		myBitmap = BitmapFactory.decodeFile(path);
        }
        
		myImage.setImageBitmap(myBitmap);
	}
	
	
	private void saveText(){
		//enregistrement dans le retourne
		mItem.setResume(textViewResume.getText().toString());	//enregistrement du texte	
		mItem.setPicturePath(pathPicture);//enregistrement du chemain
		isChange = false; //no change effective

	//	Intent result = new Intent();
	//	result.putExtra(ItemListActivity.NOTE, mItem);
	//	setResult(RESULT_OK , result);
		//enregistrement dans la BD
		dataBaseRepository.Open();
		dataBaseRepository.Update(mItem); //update the database
		dataBaseRepository.Close();
		//masquage du clavier
		InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(textViewResume.getWindowToken(), 0);
		//affichage d'un message
      	 Toast.makeText(getActivity(), "text save ", Toast.LENGTH_SHORT).show();
	}
	
	private void setNewImage(){
		Log.v("onactivityFragment", String.valueOf(RESULT_LOAD_IMAGE) );
    	Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);  //in cr�e un itent sur la vue galerie
        getActivity().startActivityForResult(i, RESULT_LOAD_IMAGE); //on demare l'activit�e galerie
	}
	
	
	
	private void deleteImage(){
		pathPicture = "";
		myBitmap.recycle();
		setImageOnImageView(pathPicture);
		myImage.setVisibility(View.GONE);	
	}
	
	public void resultLoadPicture(String pathPicture){
             
            myImage.setVisibility(View.VISIBLE); //on affiche l'image
            setImageOnImageView(pathPicture);
            this.pathPicture = pathPicture;
		
	}
	
	public boolean asChange(){

		if((mItem.getResume().equals(textViewResume.getText().toString())) && (mItem.getPicturePath().equals(pathPicture)) && isChange == false ){ //si les text et les images sont les m�mes
			return false;
		}
		else{
			return true;
		}
	}
	
	
	private void exitView()
	{
		if (myBitmap != null){
		myBitmap.recycle();
		}
	}
	
}


