package com.smartsoftasia.smartnote3.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseOpenHelper  extends SQLiteOpenHelper {
	
	// Version de la base de donn�es
	private static final int DATABASE_VERSION = 1;

	// Nom de la base
	private static final String DATEBASE_NAME = "notes.db";

	// Nom de la table
	public static final String TABLE_NAME = "Notes";

	// Description des colonnes
	public static final String KEY_ID = "id";
	public static final int NUM_COLUMN_ID = 0;
	public static final String TITLE = "title";
	public static final int NUM_COLUMN_TITLE = 1;
	public static final String SUB_TITLE = "subtitle";
	public static final int NUM_COLUMN_SUB_TITLE = 2;
	public static final String DATE = "date";
	public static final int NUM_COLUMN_DATE = 3;
	public static final String RESUME = "resume";
	public static final int NUM_COLUMN_RESUME = 4;
	public static final String PICTUREPATH = "picturePath";
	public static final int NUM_COLUMN_PICTUREPATH = 5;

	// Requ�te SQL pour la cr�ation da la base
	private static final String CREATE_DB = "CREATE TABLE "
			+ TABLE_NAME + " (" + KEY_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + TITLE
			+ " TEXT, " + SUB_TITLE + " TEXT, "
			+ DATE + " TEXT, " + RESUME +" TEXT, " + PICTUREPATH + " TEXT );";	
	
	

	public DataBaseOpenHelper(Context context, CursorFactory factory) {
		super(context, DATEBASE_NAME, factory, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(CREATE_DB);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		if (newVersion > DATABASE_VERSION) {
			db.execSQL("DROP TABLE " + TABLE_NAME + ";");
			onCreate(db);
		}
		
	}
	
	

}
