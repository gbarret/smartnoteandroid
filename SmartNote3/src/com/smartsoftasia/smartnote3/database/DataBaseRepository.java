package com.smartsoftasia.smartnote3.database;

import java.util.ArrayList;
import java.util.List;

import com.smartsoftasia.smartnote3.note.Note;



import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

public class DataBaseRepository extends Repository<Note> {
	
	public DataBaseRepository(Context context){
		sqLiteOpenHelper = new DataBaseOpenHelper(context, null);
	}

	public List<Note> GetAll() {
		// TODO Auto-generated method stub
		Cursor cursor = myDB.query(DataBaseOpenHelper.TABLE_NAME,
				new String[] { DataBaseOpenHelper.KEY_ID,
				DataBaseOpenHelper.TITLE,
				DataBaseOpenHelper.SUB_TITLE,
				DataBaseOpenHelper.DATE ,
				DataBaseOpenHelper.RESUME ,
				DataBaseOpenHelper.PICTUREPATH } , null, null, null,
				null, null);

		return ConvertCursorToListObject(cursor);
	}

	public Note GetById(int id) {
		// TODO Auto-generated method stub
		Cursor cursor = myDB.query(DataBaseOpenHelper.TABLE_NAME,
				new String[] { DataBaseOpenHelper.KEY_ID,
				DataBaseOpenHelper.TITLE,
				DataBaseOpenHelper.SUB_TITLE,
				DataBaseOpenHelper.DATE,
				DataBaseOpenHelper.RESUME,
				DataBaseOpenHelper.PICTUREPATH},
				DataBaseOpenHelper.KEY_ID + "=?",
				new String[] { String.valueOf(id) }, null, null, null);

		return ConvertCursorToObject(cursor);
	}

	public long Save(Note entite) {
		// TODO Auto-generated method stub
		ContentValues contentValues = new ContentValues();
		contentValues.put(DataBaseOpenHelper.TITLE, entite.getTitre());
		contentValues.put(DataBaseOpenHelper.SUB_TITLE,entite.getSubTitle());
		contentValues.put(DataBaseOpenHelper.DATE, entite.getDate());
		contentValues.put(DataBaseOpenHelper.RESUME, entite.getResume());
		contentValues.put(DataBaseOpenHelper.PICTUREPATH, entite.getPicturePath());

		return myDB.insert(DataBaseOpenHelper.TABLE_NAME, null, contentValues);	
	}
	

	public void Update(Note entite) {
		// TODO Auto-generated method stub
		ContentValues contentValues = new ContentValues();
		contentValues.put(DataBaseOpenHelper.TITLE, entite.getTitre());
		contentValues.put(DataBaseOpenHelper.SUB_TITLE,entite.getSubTitle());
		contentValues.put(DataBaseOpenHelper.DATE, entite.getDate());
		contentValues.put(DataBaseOpenHelper.RESUME, entite.getResume());
		contentValues.put(DataBaseOpenHelper.PICTUREPATH, entite.getPicturePath());

		myDB.update(DataBaseOpenHelper.TABLE_NAME, contentValues, DataBaseOpenHelper.KEY_ID + "=?", new String[] { String.valueOf(entite.getID()) });
		
	}

	public void Delete(int id) {
		// TODO Auto-generated method stub
		myDB.delete(DataBaseOpenHelper.TABLE_NAME, DataBaseOpenHelper.KEY_ID + "=?", new String[] { String.valueOf(id) });
		
	}

	public List<Note> ConvertCursorToListObject(Cursor c) {
		// TODO Auto-generated method stub
		List<Note> liste = new ArrayList<Note>();

		if (c.getCount() == 0)
			return liste;

		c.moveToFirst();

		do {
			Note notes = ConvertCursorToObject(c);
			liste.add(notes);
		} while (c.moveToNext());
		c.close();

		return liste;	}

	public Note ConvertCursorToObject(Cursor c) {
		// TODO Auto-generated method stub
		Note notes = new Note( c.getString(DataBaseOpenHelper.NUM_COLUMN_TITLE), c.getString(DataBaseOpenHelper.NUM_COLUMN_SUB_TITLE),c.getString(DataBaseOpenHelper.NUM_COLUMN_DATE),c.getString(DataBaseOpenHelper.NUM_COLUMN_RESUME),c.getString(DataBaseOpenHelper.NUM_COLUMN_PICTUREPATH));	
		notes.setID(c.getInt(DataBaseOpenHelper.NUM_COLUMN_ID));
		return notes;	}

	public Note ConvertCursorToOneObject(Cursor c) {
		// TODO Auto-generated method stub
		c.moveToFirst();
		Note notes = ConvertCursorToObject(c);
		c.close();
		return notes;
	}

}
