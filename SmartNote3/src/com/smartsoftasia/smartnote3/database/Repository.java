package com.smartsoftasia.smartnote3.database;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;



public abstract class Repository<T> implements IRepository<T> {
		// Base de donn�es
		protected SQLiteDatabase myDB;
		
		protected SQLiteOpenHelper sqLiteOpenHelper;
		
		/**
		 * constructor
		 */
		public Repository() {
			
		}
		
		/**
		 * Open connection
		 */
		public void Open() {
			myDB = sqLiteOpenHelper.getWritableDatabase();
		}

		/**
		 * close connection
		 */
		public void Close() {
			myDB.close();
		}

}
