package com.smartsoftasia.smartnote3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.ContextMenu.ContextMenuInfo;

import com.smartsoftasia.smartnote3.database.DataBaseRepository;
import com.smartsoftasia.smartnote3.R;
import com.smartsoftasia.smartnote3.note.Note;


public class ItemListFragment extends ListFragment {
	
	protected static String NOTE = "com.smartdoftasia.smartnote3.NOTE";
	// L'identifiant de notre requ�te
	public final static int SHOW_NOTE = 0;
	private List<Note> mesNotes  = new ArrayList<Note>();;
	private ArrayAdapter<Note> adapter;
	private DataBaseRepository dataBaseRepository;
	private int positionNoteOld=-1;




	private static final String STATE_ACTIVATED_POSITION = "activated_position";

	private Callbacks mCallbacks = sDummyCallbacks;


	private int mActivatedPosition = ListView.INVALID_POSITION;


	public interface Callbacks {
		/**
		 * Callback for when an item has been selected.
		 */
	//	public void onItemSelected(String id);

		public void onItemSelected(Note id);
	}

	/**
	 * A dummy implementation of the {@link Callbacks} interface that does
	 * nothing. Used only when this fragment is not attached to an activity.
	 */
	private static Callbacks sDummyCallbacks = new Callbacks() {
	//	@Override
	//	public void onItemSelected(String id) {
	//	}
		@Override
		public void onItemSelected(Note id) {
		}
	};

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public ItemListFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// TODO: replace with a real list adapter.       
        setHasOptionsMenu(true);

	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		// Restore the previously serialized activated item position.
		if (savedInstanceState != null
				&& savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
			setActivatedPosition(savedInstanceState
					.getInt(STATE_ACTIVATED_POSITION));
		}
		
		initialization();
		
		//save the list to the contextual menu
		registerForContextMenu(getListView());
		
		
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// Activities containing this fragment must implement its callbacks.
		if (!(activity instanceof Callbacks)) {
			throw new IllegalStateException(
					"Activity must implement fragment's callbacks.");
		}

		mCallbacks = (Callbacks) activity;
	}

	@Override
	public void onDetach() {
		super.onDetach();

		// Reset the active callbacks interface to the dummy implementation.
		mCallbacks = sDummyCallbacks;
	}

	@Override
	public void onListItemClick(ListView listView, View view, int position,
			long id) {
		super.onListItemClick(listView, view, position, id);
		


			// Notify the active callbacks interface (the activity, if the
			// fragment is attached to one) that an item has been selected.
			mCallbacks.onItemSelected(mesNotes.get(position));
	}
	



	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (mActivatedPosition != ListView.INVALID_POSITION) {
			// Serialize and persist the activated item position.
			outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
		}
	}
	

	/**
	 * Turns on activate-on-click mode. When this mode is on, list items will be
	 * given the 'activated' state when touched.
	 */
	public void setActivateOnItemClick(boolean activateOnItemClick) {
		// When setting CHOICE_MODE_SINGLE, ListView will automatically
		// give items the 'activated' state when touched.
		getListView().setChoiceMode(
				activateOnItemClick ? ListView.CHOICE_MODE_SINGLE
						: ListView.CHOICE_MODE_NONE);
	}

	private void setActivatedPosition(int position) {
		if (position == ListView.INVALID_POSITION) {
			getListView().setItemChecked(mActivatedPosition, false);
		} else {
			getListView().setItemChecked(position, true);
		}

		mActivatedPosition = position;
	}
	
	/**********************************************************************************************************/

	@Override	
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.item_list_activity, menu);
    }
	
    /*
     * When we touch the menu (up and down)
     * @see android.app.Activity#onMenuItemSelected(int, android.view.MenuItem)
     */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// We get the ID of the button and we see case
		switch (item.getItemId()) {
			case R.id.buttonAdd: //from the menu button
				addItems();
		}
		return super.onOptionsItemSelected(item);
	}
	
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
	    super.onCreateContextMenu(menu, v, menuInfo);
	    menu.setHeaderTitle("Menu");
	    android.view.MenuInflater inflater = (android.view.MenuInflater) getActivity().getMenuInflater();
	    inflater.inflate(R.menu.menu_contextual_listactivity, (android.view.Menu) menu);
    }


	
	@Override
	public boolean onContextItemSelected(android.view.MenuItem item) {
	    AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		//get the ID of the menu, the first call is the first execute
		if(item.getGroupId() == R.id.group_menulist){
			// we get the ID and we compare
			switch (item.getItemId()) {
			
				case R.id.itemOpen: //start new activity
					mCallbacks.onItemSelected(mesNotes.get(info.position));
					return true;
	
				case R.id.itemDelete: //delete the item
					this.deleteElements(info.position);
					return true;
					
				case R.id.itemCancel: //do nothing
					return true;
			}
		}
		return super.onContextItemSelected(item);
	}
	
	/*****************************************************************************************************************************/
	
    private void initialization(){
    	//get the layout

        dataBaseRepository = new DataBaseRepository(getView().getContext()); //create nex database
        mesNotes.clear(); //useless but just for know
    	dataBaseRepository.Open();
        mesNotes = dataBaseRepository.GetAll(); //we get all the database an put it on an array
    	dataBaseRepository.Close();
    	Collections.reverse(mesNotes); //on inverse la liste
	

        adapter = new ArrayAdapter<Note>(getActivity(),
        android.R.layout.simple_list_item_activated_1,android.R.id.text1, mesNotes);
        setListAdapter(adapter);

    	
    }
    
	private void addItems(){
		// TODO Auto-generated method stub
		AlertDialog.Builder dialogBoxNew;
        final EditText input = new EditText(getActivity());
        dialogBoxNew = new AlertDialog.Builder(getActivity());
        dialogBoxNew.setView(input);
        dialogBoxNew.setTitle("New note");
        dialogBoxNew.setIcon(R.drawable.ic_launcher);
        dialogBoxNew.setMessage("Enter title");
        
        dialogBoxNew.setPositiveButton("ok", new DialogInterface.OnClickListener() {
           
            public void onClick(DialogInterface dialog, int which) {
            	String value = input.getText().toString();
            	if(value.length() <= 0){
            		Toast.makeText(getActivity(), "your title is empty ", Toast.LENGTH_SHORT).show();
            	}
            	else{

    				 mesNotes.add(0, new Note(value,"",null,"","")); //add at the begening of the list
             		 dataBaseRepository.Open();
    				 mesNotes.get(0).setID((int)dataBaseRepository.Save(mesNotes.get(0)));
                  	 dataBaseRepository.Close();
                 	 adapter.notifyDataSetChanged();
                   	 Toast.makeText(getActivity(), "new note create ", Toast.LENGTH_SHORT).show();
            	}
           
            }
            });
        dialogBoxNew.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
           
            public void onClick(DialogInterface dialog, int which) {

            }
            });
        dialogBoxNew.show();
        adapter.notifyDataSetChanged();
		}
	
	private void deleteElements(int position)
	{
	    dataBaseRepository.Open();
	    dataBaseRepository.Delete(mesNotes.get(position).getID());
	    dataBaseRepository.Close();
		mesNotes.remove(position);
        adapter.notifyDataSetChanged();
        Toast.makeText(getActivity(), "Note delete ", Toast.LENGTH_SHORT).show();

	}
	
	/*****************************************************************************************************************************************************************/
	
	public void setSelectItem(){
		getListView().setItemChecked(this.positionNoteOld, true);
	}


	
	
	
}
