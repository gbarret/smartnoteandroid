package com.smartsoftasia.smartnote3;

import java.util.List;

import com.smartsoftasia.smartnote3.R;
import com.smartsoftasia.smartnote3.note.Note;

import android.app.AlertDialog;
import android.app.ListFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.ListView;

/**
 * An activity representing a list of Items. This activity has different
 * presentations for handset and tablet-size devices. On handsets, the activity
 * presents a list of items, which when touched, lead to a
 * {@link ItemDetailActivity} representing item details. On tablets, the
 * activity presents the list of items and item details side-by-side using two
 * vertical panes.
 * <p>
 * The activity makes heavy use of fragments. The list of items is a
 * {@link ItemListFragment} and the item details (if present) is a
 * {@link ItemDetailFragment}.
 * <p>
 * This activity also implements the required {@link ItemListFragment.Callbacks}
 * interface to listen for item selections.
 */
public class ItemListActivity extends FragmentActivity implements
		ItemListFragment.Callbacks {

	/**
	 * Whether or not the activity is in two-pane mode, i.e. running on a tablet
	 * device.
	 */
	private boolean mTwoPane;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_item_list);

		if (findViewById(R.id.item_detail_container) != null) {
			// The detail container view will be present only in the
			// large-screen layouts (res/values-large and
			// res/values-sw600dp). If this view is present, then the
			// activity should be in two-pane mode.
			mTwoPane = true;

			// In two-pane mode, list items should be given the
			// 'activated' state when touched.
			((ItemListFragment) getSupportFragmentManager().findFragmentById(
					R.id.item_list)).setActivateOnItemClick(true);
			
		}



		// TODO: If exposing deep links into your app, handle intents here.
	}
	
	//public void onActivityCreated(Bundle savedState) {
	//	ListView liste = (ListView) findViewById (R.id.item_list);
	//	registerForContextMenu();	}

	/**
	 * Callback method from {@link ItemListFragment.Callbacks} indicating that
	 * the item with the given ID was selected.
	 */
	@Override
	public void onItemSelected(Note id) {
		if (mTwoPane) {
			if(getSupportFragmentManager().findFragmentById(R.id.item_detail_container) != null){
				finishview(id);
			}
			else{
				changeFrangmentDetail (id);
			}

			
		} else {
			// In single-pane mode, simply start the detail activity
			// for the selected item ID.
			Intent detailIntent = new Intent(this, ItemDetailActivity.class);
			detailIntent.putExtra(ItemDetailFragment.ARG_ITEM_ID, id);
			startActivity(detailIntent);
		}
	}
	
	//retour de la galerie d'image
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
         
        if (requestCode == ItemDetailFragment.RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
 
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
 
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String pathPicture = cursor.getString(columnIndex);
            cursor.close();
             
            ItemDetailFragment myFragment = (ItemDetailFragment) getSupportFragmentManager().findFragmentById(R.id.item_detail_container);
            myFragment.resultLoadPicture(pathPicture);
            

        	}


    	}
    
    /****************************************************************************************************************************************************/
    
    public void finishview(final Note id){

		if(!((ItemDetailFragment) getSupportFragmentManager().findFragmentById(R.id.item_detail_container)).asChange()){ //si les text et les images sont les m�mes
			changeFrangmentDetail(id);
		}
		else{
			AlertDialog.Builder dialogBoxNew;
            dialogBoxNew = new AlertDialog.Builder(this);
            dialogBoxNew.setTitle("Exit");
            dialogBoxNew.setIcon(R.drawable.ic_launcher);
            dialogBoxNew.setMessage("note as been modify, quit without saving?");
            
            dialogBoxNew.setPositiveButton("ok", new DialogInterface.OnClickListener() {
               
                public void onClick(DialogInterface dialog, int which) {
                	changeFrangmentDetail(id);
               }
                });
            
            dialogBoxNew.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                
                public void onClick(DialogInterface dialog, int which) {
                	//select the previous item
                //	((ItemListFragment) getSupportFragmentManager().findFragmentById(R.id.item_list)).setSelectItem(position) ;
                }
                });
            dialogBoxNew.show();  
		}
		
	}
    private void exitView(){
		NavUtils.navigateUpTo(this,
				new Intent(this, ItemListActivity.class));
    }
    
    private void changeFrangmentDetail (Note id)
    {
    	
		// In two-pane mode, show the detail view in this activity by
		// adding or replacing the detail fragment using a
		// fragment transaction.
		Bundle arguments = new Bundle();
		arguments.putParcelable(ItemDetailFragment.ARG_ITEM_ID, id);
		ItemDetailFragment fragment = new ItemDetailFragment();
		fragment.setArguments(arguments);
		getSupportFragmentManager().beginTransaction()
				.replace(R.id.item_detail_container, fragment).commit();
		
    	
    }
    
	
	
}
