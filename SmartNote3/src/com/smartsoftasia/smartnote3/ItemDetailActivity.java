package com.smartsoftasia.smartnote3;

import com.smartsoftasia.smartnote3.ItemListActivity;
import com.smartsoftasia.smartnote3.R;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;

/**
 * An activity representing a single Item detail screen. This activity is only
 * used on handset devices. On tablet-size devices, item details are presented
 * side-by-side with a list of items in a {@link ItemListActivity}.
 * <p>
 * This activity is mostly just a 'shell' activity containing nothing more than
 * a {@link ItemDetailFragment}.
 */
public class ItemDetailActivity extends FragmentActivity {
	
	private static int RESULT_LOAD_IMAGE = 1;



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_item_detail);
		
		if (savedInstanceState == null) {

		// Show the Up button in the action bar.
		getActionBar().setDisplayHomeAsUpEnabled(true);

		// savedInstanceState is non-null when there is fragment state
		// saved from previous configurations of this activity
		// (e.g. when rotating the screen from portrait to landscape).
		// In this case, the fragment will automatically be re-added
		// to its container so we don't need to manually add it.
		// For more information, see the Fragments API guide at:
		//
		// http://developer.android.com/guide/components/fragments.html
		//
			// Create the detail fragment and add it to the activity
			// using a fragment transaction.
		
    	Intent i = getIntent();

		
			Bundle arguments = new Bundle();
			arguments.putParcelable(ItemDetailFragment.ARG_ITEM_ID, i.getParcelableExtra(ItemDetailFragment.ARG_ITEM_ID));
			ItemDetailFragment fragment = new ItemDetailFragment();
			fragment.setArguments(arguments);
			getSupportFragmentManager().beginTransaction()
					.add(R.id.item_detail_container, fragment).commit();
		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			
			finishview();

			return true;
			
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed() {
		finishview();
		}
	
	/*******************************************************************************************************************************************/
	
	//retour de la galerie d'image
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

         
        if ( requestCode == ItemDetailFragment.RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {

        	
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
 
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
 
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String pathPicture = cursor.getString(columnIndex);
            cursor.close();
             
            ItemDetailFragment myFragment = (ItemDetailFragment) getSupportFragmentManager().findFragmentById(R.id.item_detail_container);
            myFragment.resultLoadPicture(pathPicture);
            

        	}

    	}
    
    /****************************************************************************************************************************************************/
    
    public void finishview(){

		if(!((ItemDetailFragment) getSupportFragmentManager().findFragmentById(R.id.item_detail_container)).asChange()){ //si les text et les images sont les m�mes
			exitView();
		}
		else{
			AlertDialog.Builder dialogBoxNew;
            dialogBoxNew = new AlertDialog.Builder(this);
            dialogBoxNew.setTitle("Exit");
            dialogBoxNew.setIcon(R.drawable.ic_launcher);
            dialogBoxNew.setMessage("note as been modify, quit without saving?");
            
            dialogBoxNew.setPositiveButton("ok", new DialogInterface.OnClickListener() {
               
                public void onClick(DialogInterface dialog, int which) {
                	exitView();
               }
                });
            
            dialogBoxNew.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                
                public void onClick(DialogInterface dialog, int which) {
                }
                });
            dialogBoxNew.show();  
		}
		
	}
    private void exitView(){
		NavUtils.navigateUpTo(this,
				new Intent(this, ItemListActivity.class));
    }
    
}
