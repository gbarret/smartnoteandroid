package com.smartsoftasia.smartnote3.note;


import android.os.Parcel;
import android.os.Parcelable;
import android.text.format.Time;

public class Note implements Parcelable {
	
	private int ID;
	private String titre;
	private String subTitle;
	private String resume;
	private String date;
	private String picturePath;
	
	public Note(String titre, String subTitle, String Date, String resume , String picturePath){
		this.titre = titre;
		this.subTitle = subTitle;
		if(Date != null){
			this.date = Date;
		}
		else{
			Time tmpDate = new Time() ;
			tmpDate.setToNow();
			this.date = tmpDate.format("%Y-%m-%d %H:%M");
		}
		this.resume = resume;
		this.picturePath = picturePath;
	}
	

	public int describeContents() {
	//On renvoie 0, car notre classe ne contient pas de FileDescriptor
	return 0;
	}

	public void writeToParcel(Parcel dest, int flags) {
	// On ajoute les objets dans l'ordre dans lequel on les a d�clar�s
	dest.writeInt(ID);
	dest.writeString(titre);
	dest.writeString(subTitle);
	dest.writeString(resume);
	dest.writeString(date);
	dest.writeString(picturePath);
	}
	
	public static final Parcelable.Creator<Note> CREATOR = new Parcelable.Creator<Note>() {

			public Note createFromParcel(Parcel source) {
			return new Note(source);
			}

			public Note[] newArray(int size) {
			return new Note[size];
			}
			};
	public Note(Parcel in) {
		ID = in.readInt();
		titre = in.readString();
		subTitle = in.readString();
		resume = in.readString();
		date = in.readString();
		picturePath = in.readString();
	}
			

	public int getID(){
		return ID;
	}
	
	public void setID(int ID){
		this.ID = ID;
	}
	
	public String getTitre(){
		return titre;
	}
	
	public void setTitre(String titre){
		this.titre = titre;
	}
	
	public String getSubTitle(){
		return subTitle;
	}
	
	public void setSubTitle(String subTitle){
		this.subTitle = subTitle;
	}
	
	public String getResume(){
		return resume;
	}
	
	public void setResume(String resume){
		this.resume = resume;
	}
	
	public String getDate(){
		return this.date;
	}	
	
	public void setDate(){
		Time tmpDate = new Time() ;
		tmpDate.setToNow();
		this.date = tmpDate.format("%Y-%m-%d %H:%M");
	}
	
	public String getPicturePath(){
		return this.picturePath;
	}
	
	public void setPicturePath(String picturePath){
		this.picturePath = picturePath;
	}
	
	@Override
	public String toString() {
		return this.titre;
	}

	
}
