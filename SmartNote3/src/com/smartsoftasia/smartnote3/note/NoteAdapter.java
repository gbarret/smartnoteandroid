package com.smartsoftasia.smartnote3.note;



import java.util.List;

import com.smartsoftasia.smartnote3.ItemListFragment;
import com.smartsoftasia.smartnote3.R;
import com.smartsoftasia.smartnote3.R.id;
import com.smartsoftasia.smartnote3.R.layout;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class NoteAdapter extends BaseAdapter {
	
	private List<Note> lesNotes;
	private LayoutInflater inflater;
	
	private class ViewHolder{
		TextView titre;
		TextView date;
	}
	
	public NoteAdapter(Context context, List<Note> lesNotes){
		inflater = LayoutInflater.from(context);
		this.lesNotes = lesNotes;
	}

	public int getCount() {
		// TODO Auto-generated method stub
		return lesNotes.size();
	}

	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return lesNotes.get(arg0);
	}

	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}
	


	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		ViewHolder holder;
		if(convertView == null) {
		holder = new ViewHolder();
		convertView = inflater.inflate(R.layout.sub_item_list , null);
		holder.titre = (TextView)convertView.findViewById(R.id.title);
		holder.date = (TextView)convertView.findViewById(R.id.resume);
		convertView.setTag(holder);
		} else {
		holder = (ViewHolder) convertView.getTag();
		}
		holder.titre.setText(lesNotes.get(position).getTitre());
		holder.date.setText(lesNotes.get(position).getDate());
		return convertView;

	}


}

